FROM bluerain/java-10-buildpack:jre


COPY ./build/libs/zhclean-server-0.1-SNAPSHOT.jar /home/app/start-web.jar


WORKDIR /home/app


EXPOSE 8080


ENTRYPOINT ["java", "-jar", "start-web.jar"]