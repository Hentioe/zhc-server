package io.bluerain.zhclean.controllers;

import io.bluerain.zhclean.models.Feed;
import io.bluerain.zhclean.services.FeedService;
import io.reactivex.schedulers.Schedulers;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@EnableAutoConfiguration
@RequestMapping("/feed")
public class FeedController {

    private FeedService feedService = new FeedService();


    @GetMapping("")
    Feed index() throws ExecutionException, InterruptedException {
        return byPage(1);
    }

    @GetMapping("/{page}")
    @ResponseBody
    Feed byPage(@PathVariable("page") int page)
            throws ExecutionException, InterruptedException {
        return feedService.getList(page).subscribeOn(Schedulers.io()).observeOn(Schedulers.computation()).toFuture().get();
    }
}
